package org.testing;
import org.example.Sorting;
import org.junit.Test;

public class NullTesting {
    Sorting sorting = new Sorting();

    /**
     * tests if Sorting.java sort class throws illegal argument when parameter is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void NullCaseTest() {
        sorting.sort(null);
    }

}
