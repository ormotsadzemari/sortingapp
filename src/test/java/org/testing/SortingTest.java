package org.testing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.Sorting;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class SortingTest {
    private static final Logger logger = LogManager.getLogger(SortingTest.class);

    int[] arr;
    int[] expected;
    Sorting sorting = new Sorting();

    /**
     * it's a constructor of org.testing.SortingTest class, which takes array
     * of integers and for parametrizedTest method creates other
     * array which should be right answer for tests.
     *
     * @param arr is an array of integers
     */
    public SortingTest(int[] arr) {
        this.arr = arr;
        int[] copyOf=Arrays.copyOf(arr,arr.length);
        Arrays.sort(copyOf);
        this.expected = copyOf;
    }

    /**
     * it creates collection of arrays to be used in parametrizedTest.
     *
     * @return collection of arrays of integers
     */
    @Parameterized.Parameters
    public static Collection<int[]> data() {
        return Arrays.asList(new int[][]{{}, {1}, {8, 9, -1, 4}, {1, 1, 2, 3, 5, 6, 1, 3, 41, 6, 7, 2, 0}, {5, 6, Integer.MAX_VALUE, 4, 2, 4, 5, 6, Integer.MIN_VALUE, -9}, {Integer.MAX_VALUE, 1000066584, 999537, -80987543, -654480}});
    }

    @Test
    public void parameterizedTest() {
        logger.info("Sorting array: " + Arrays.toString(arr));
        sorting.sort(arr);
        logger.info("Sorted array: "+ Arrays.toString(arr));
        Assertions.assertArrayEquals(expected, arr);
    }


}
