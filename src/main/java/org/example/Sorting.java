package org.example;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;


public class Sorting {

    public static void main(String[] args) {

    }

    /**
     * it sorts array asc. and throws illegal argument exception if arr is null
     *
     * @param arr array of integers
     */
    public void sort(int[] arr) {
        if (arr == null) {
            throw new IllegalArgumentException();
        }
        Arrays.sort(arr);

    }

}
